package SQL;

public class Song {
	int id = 0;
	String name = null;
	String singer = null;
	String language= null; 
	
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
		
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getSinger() {
		return singer;
	}
	public String getlanguage() {
		return language;
	}

}
