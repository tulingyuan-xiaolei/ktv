package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.Song;
import SQL.SqlServer;

import javax.swing.JLabel;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JScrollPane;

public class SingerSearch extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	JList list;
	String name;
	String singer;
	String language;
	static int index3;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { PlayerSearch frame = new
	 * PlayerSearch(); frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */

	/**
	 * Create the frame.
	 */
	public String[] getValues() {//获取列表
		SqlServer sqlServer = new SqlServer();
		List<Song> sList = sqlServer.getSongList();
		String str[] = new String[sList.size()];
		Song s = null;
		for(int i=0;i<sList.size();i++) {
			s = sList.get(i);
			str[i]  =s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
			
		}
		return str;
	
	}
	public String getName() {
    	return name;
    }
    public String getSinger() {
    	return singer;
    }
    public String getLanguage() {
    	return language;
    }
   
	public SingerSearch() {
		SingerSearch singersearch = this;//保存SongSearch的数据
		StringBuilder strbuild = new StringBuilder();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 645, 691);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
		lblNewLabel.setBounds(0, 0, 621, 138);
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(106, 419, 209, 35);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("\u6B4C\u624B\u9996\u5B57\u6BCD");
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 17));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(0, 419, 99, 24);
		panel.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("\u9000\u683C");
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 139, 621, 272);
		panel.add(scrollPane);
		
		JList list = new JList();
		list.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        	    index3= list.getSelectedIndex();
        		int x = e.getX();
				int y = e.getY();
				int rx = x + 10 + 100 +10;
				int ry = y + 52 + 100 +110;
        		WindowsAdd winadd= new WindowsAdd();
        		winadd.setLocation(rx,ry);
        		
        		Song s = null;
        		List<Song> sList= list.getSelectedValuesList();//获得点击的信息【】
        		String string = sList.toString();
        		String regex = "\\s+";
        		
        		String[] name1  = string.split(regex);
        		name = name1[0].substring(1,name1[0].length());
        		singer = name1[1];
        		language = name1[2].substring(0,name1[2].length()-1);
        		winadd.setSingerSearch(singersearch);//使用windowsadd的方法传参 
        		winadd.getFlag(2);
        	}
        });
		scrollPane.setViewportView(list);
		list.setFont(new Font("宋体", Font.PLAIN, 18));
		scrollPane.setViewportView(list);
        list.setModel(new AbstractListModel() {//获取全部
			
			String[] values  = getValues();
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				
				return values[index];
			}
			
		});
	
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				strbuild.deleteCharAt(strbuild.length()-1);
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
				}catch(Exception e1) {
					JOptionPane.showMessageDialog(panel,"输入框已为空","消息对话框",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(491, 590, 113, 54);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u8FD4\u56DE");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Play();
				dispose();
			}
		});
		btnNewButton_1.setBounds(475, 417, 129, 35);
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("\u6E05\u7A7A");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.delete(0, strbuild.length());
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_2.setBounds(325, 417, 140, 35);
		panel.add(btnNewButton_2);
		
		
		
		JButton btnNewButton_3 = new JButton("Q");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("Q");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		
		btnNewButton_3.setFont(new Font("宋体", Font.PLAIN, 29));
		
		btnNewButton_3.setBounds(0, 462, 52, 54);
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_3_1 = new JButton("W");
		
		btnNewButton_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("W");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1.setBounds(62, 462, 52, 54);
		panel.add(btnNewButton_3_1);
		
		JButton btnNewButton_3_1_1 = new JButton("E");
		btnNewButton_3_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("E");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1.setBounds(124, 462, 52, 54);
		panel.add(btnNewButton_3_1_1);
		
		JButton btnNewButton_3_1_1_1 = new JButton("R");
		btnNewButton_3_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("R");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_1.setBounds(186, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_1);
		
		JButton btnNewButton_3_1_1_2 = new JButton("T");
		btnNewButton_3_1_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("T");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_2.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_2.setBounds(248, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_2);
		
		JButton btnNewButton_3_1_1_3 = new JButton("Y");
		btnNewButton_3_1_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("Y");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_3.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_3.setBounds(310, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_3);
		
		JButton btnNewButton_3_1_1_4 = new JButton("U");
		btnNewButton_3_1_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("U");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_4.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_4.setBounds(373, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_4);
		
		JButton btnNewButton_3_1_1_5 = new JButton("I");
		btnNewButton_3_1_1_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("I");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_5.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_5.setBounds(435, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_5);
		
		JButton btnNewButton_3_1_1_6 = new JButton("O");
		btnNewButton_3_1_1_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("O");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_1_1_6.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_6.setBounds(497, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_6);
		
		JButton btnNewButton_3_1_1_6_1 = new JButton("P");
		btnNewButton_3_1_1_6_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("P");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
				
			}
		});
		btnNewButton_3_1_1_6_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_1_1_6_1.setBounds(559, 462, 52, 54);
		panel.add(btnNewButton_3_1_1_6_1);
		
		JButton btnNewButton_3_2 = new JButton("A");
		btnNewButton_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("A");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2.setBounds(23, 526, 52, 54);
		panel.add(btnNewButton_3_2);
		
		JButton btnNewButton_3_2_1 = new JButton("S");
		btnNewButton_3_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("S");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1.setBounds(95, 526, 52, 54);
		panel.add(btnNewButton_3_2_1);
		
		JButton btnNewButton_3_2_1_1 = new JButton("D");
		btnNewButton_3_2_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("D");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_1.setBounds(166, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_1);
		
		JButton btnNewButton_3_2_1_2 = new JButton("F");
		btnNewButton_3_2_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("F");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2.setBounds(228, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2);
		
		JButton btnNewButton_3_2_1_2_1 = new JButton("G");
		btnNewButton_3_2_1_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("G");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2_1.setBounds(295, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2_1);
		
		JButton btnNewButton_3_2_1_2_1_1 = new JButton("H");
		btnNewButton_3_2_1_2_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("H");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2_1_1.setBounds(357, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2_1_1);
		
		JButton btnNewButton_3_2_1_2_1_1_1 = new JButton("J");
		btnNewButton_3_2_1_2_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("J");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2_1_1_1.setBounds(419, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2_1_1_1);
		
		JButton btnNewButton_3_2_1_2_1_1_1_1 = new JButton("K");
		btnNewButton_3_2_1_2_1_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("K");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2_1_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2_1_1_1_1.setBounds(481, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2_1_1_1_1);
		
		JButton btnNewButton_3_2_1_2_1_1_1_1_1 = new JButton("L");
		btnNewButton_3_2_1_2_1_1_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("L");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_1_2_1_1_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_1_2_1_1_1_1_1.setBounds(543, 526, 52, 54);
		panel.add(btnNewButton_3_2_1_2_1_1_1_1_1);
		
		JButton btnNewButton_3_2_2 = new JButton("Z");
		btnNewButton_3_2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("Z");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
				
				
			}
		});
		btnNewButton_3_2_2.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_2.setBounds(33, 590, 52, 54);
		panel.add(btnNewButton_3_2_2);
		
		JButton btnNewButton_3_2_3 = new JButton("X");
		btnNewButton_3_2_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("X");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3.setBounds(105, 590, 52, 54);
		panel.add(btnNewButton_3_2_3);
		
		JButton btnNewButton_3_2_3_1 = new JButton("C");
		btnNewButton_3_2_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("C");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3_1.setBounds(166, 590, 52, 54);
		panel.add(btnNewButton_3_2_3_1);
		
		JButton btnNewButton_3_2_3_1_1 = new JButton("V");
		btnNewButton_3_2_3_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("V");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3_1_1.setBounds(228, 590, 52, 54);
		panel.add(btnNewButton_3_2_3_1_1);
		
		JButton btnNewButton_3_2_3_1_1_1 = new JButton("B");
		btnNewButton_3_2_3_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("B");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3_1_1_1.setBounds(295, 590, 52, 54);
		panel.add(btnNewButton_3_2_3_1_1_1);
		
		JButton btnNewButton_3_2_3_1_1_1_1 = new JButton("N");
		btnNewButton_3_2_3_1_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("N");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3_1_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3_1_1_1_1.setBounds(357, 590, 52, 54);
		panel.add(btnNewButton_3_2_3_1_1_1_1);
		
		JButton btnNewButton_3_2_3_1_1_1_1_1 = new JButton("M");
		btnNewButton_3_2_3_1_1_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.append("M");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSinger(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton_3_2_3_1_1_1_1_1.setFont(new Font("宋体", Font.PLAIN, 29));
		btnNewButton_3_2_3_1_1_1_1_1.setBounds(429, 590, 52, 54);
		panel.add(btnNewButton_3_2_3_1_1_1_1_1);
		
		
		
		
		
		
		setVisible(true);
	}
}
