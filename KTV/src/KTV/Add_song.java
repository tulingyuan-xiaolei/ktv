package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SqlServer;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Add_song extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Add_song frame = new Add_song();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */
	/**
	 * Create the frame.
	 */
	public Add_song() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 478, 423);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u6B4C\u66F2\u540D");
		lblNewLabel.setBounds(43, 20, 58, 15);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6F14\u5531\u8005");
		lblNewLabel_1.setBounds(43, 75, 58, 15);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u6B4C\u66F2\u8BED\u79CD");
		lblNewLabel_2.setBounds(43, 129, 58, 15);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u6B4C\u66F2\u7F29\u5199");
		lblNewLabel_3.setBounds(43, 176, 58, 15);
		panel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("\u6B4C\u624B\u7F29\u5199");
		lblNewLabel_4.setBounds(43, 221, 58, 15);
		panel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("\u6B4C\u66F2\u8D44\u6E90");
		lblNewLabel_5.setBounds(43, 266, 58, 15);
		panel.add(lblNewLabel_5);
		
		JButton btnNewButton = new JButton("\u6D4F\u89C8");
		btnNewButton.setBounds(301, 260, 97, 23);
		panel.add(btnNewButton);
		
		
		
		JButton btnNewButton_2 = new JButton("\u91CD\u7F6E");
		btnNewButton_2.setBounds(198, 331, 97, 23);
		panel.add(btnNewButton_2);
		
		textField = new JTextField();
		textField.setBounds(131, 17, 145, 21);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(131, 75, 145, 21);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(131, 126, 145, 21);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(131, 173, 145, 21);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(131, 218, 145, 21);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(131, 263, 145, 21);
		panel.add(textField_5);
		
		JLabel lblNewLabel_6 = new JLabel("\u6B4C\u624B\u8D44\u6E90");
		lblNewLabel_6.setBounds(43, 291, 58, 30);
		panel.add(lblNewLabel_6);
		
		textField_6 = new JTextField();
		textField_6.setBounds(131, 294, 145, 21);
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		JButton btnNewButton_3 = new JButton("\u9000\u51FA");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Add_admin();
				dispose();
			}
		});
		btnNewButton_3.setBounds(323, 331, 97, 23);
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_1 = new JButton("\u6DFB\u52A0");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SqlServer sqlserver = new SqlServer();
				sqlserver.AddSongList(textField.getText(),textField_1.getText(),textField_2.getText(),textField_3.getText(),textField_4.getText() );
				JOptionPane.showMessageDialog(panel,"添加成功","消息对话框",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnNewButton_1.setBounds(72, 331, 97, 23);
		panel.add(btnNewButton_1);
		setVisible(true);
	}
}
