package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.SwingConstants;
import java.awt.Color;
import Music.*;
import SQL.Song;
import SQL.SqlOrder;
import SQL.SqlServer;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SongOder extends JFrame {

	private JPanel contentPane;
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { SongOder frame = new SongOder();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */
	/**
	 * Create the frame.
	 */
	public void index(int index5) {//优先
		String str[] = this.getValues();
	    String temp = new String();
	    for(int i= index5;i>0;i--) {
	      temp = str[i-1];
	      str[i-1] = str[i];
	      str[i] = temp;
			 
	  }
	    list.setListData(str);
	}
	public String[] getValues() {//获取列表
		SqlOrder sqlorder = new SqlOrder();
		List<Song> sList = sqlorder.getSongList();
		String str[] = new String[sList.size()];
		Song s = null;
		for(int i=0;i<sList.size();i++) {
			s = sList.get(i);
			str[i]  =s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
		}
		return str;
	
	}
	static int index5;
    boolean Playing = true;
    int i = 0;
	JList list;
    StringBuilder strbuild = new StringBuilder();
	PlayMusic playmusic= new PlayMusic();
	private JTextField textField;
	public SongOder() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				playmusic.action("关闭");
			}
		});
		
	        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        setBounds(100, 100, 645, 691);
	        contentPane = new JPanel();
	        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	        setContentPane(contentPane);
	        contentPane.setLayout(null);
	        
	        JPanel panel = new JPanel();
	        panel.setBounds(0, 0, 631, 149);
	        contentPane.add(panel);
	        panel.setLayout(null);
	        
	        JLabel lblNewLabel = new JLabel("");
	        lblNewLabel.setIcon(new ImageIcon("image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
	        lblNewLabel.setBounds(0, 0, 631, 149);
	        panel.add(lblNewLabel);
	        tabbedPane.setBounds(0, 150, 631, 309);
	        contentPane.add(tabbedPane);
	        
	        JScrollPane scrollPane = new JScrollPane();
	        tabbedPane.addTab("\u5DF2\u70B9\u6B4C\u66F2", null, scrollPane, null);
	        
	        list = new JList();
	        list.setFont(new Font("宋体", Font.PLAIN, 18));
	        list.setModel(new AbstractListModel() {
	        	
	        	String[] values = getValues();
	        	public int getSize() {
	        		return values.length;
	        	}
	        	public Object getElementAt(int index) {
	        		return values[index];
	        	}
	        });
	        scrollPane.setViewportView(list);
	        
	        JScrollPane scrollPane_1 = new JScrollPane();
	        tabbedPane.addTab("\u5DF2\u5531\u6B4C\u66F2", null, scrollPane_1, null);
	        
	        JList list_1 = new JList();
	        scrollPane_1.setViewportView(list_1);
	        
	        JPanel panel_1 = new JPanel();
	        panel_1.setBackground(Color.WHITE);
	        panel_1.setBounds(0, 461, 631, 193);
	        contentPane.add(panel_1);
	        panel_1.setLayout(null);
	        
	        String[] songorder = getValues();//获取歌单
	        playmusic.Play("music//"+songorder[i]+".wav");//歌单第一个播放
	        
	        JLabel lblNewLabel_3_1_1 = new JLabel("\u64AD\u653E");
	        lblNewLabel_3_1_1.setIcon(new ImageIcon("image\\开始播放.png"));
	        lblNewLabel_3_1_1.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		
	        		if(Playing) {
	        			playmusic.action("播放");
						Playing = false;
						lblNewLabel_3_1_1.setIcon(new ImageIcon("image\\暂停播放.png"));
						
						textField.setText(songorder[i]);
					}else {
						playmusic.action("暂停");
						Playing = true;
						lblNewLabel_3_1_1.setIcon(new ImageIcon("image\\开始播放.png"));
					}
	        	}
	        });
	        lblNewLabel_3_1_1.setHorizontalAlignment(SwingConstants.CENTER);
	        lblNewLabel_3_1_1.setBounds(262, 90, 75, 75);
	        panel_1.add(lblNewLabel_3_1_1);
	        
	        JLabel lblNewLabel_3 = new JLabel("\u91CD\u64AD");
	        lblNewLabel_3.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		playmusic.action("暂停");
	        		playmusic.Play("music\\"+songorder[i]+".wav");
	        		playmusic.action("播放");
	        		textField.setText(songorder[i]);
	        	}
	        });
	        lblNewLabel_3.setIcon(new ImageIcon("image\\重唱.png"));
	        lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
	        lblNewLabel_3.setBounds(41, 90, 75, 75);
	        panel_1.add(lblNewLabel_3);
	        
	        JLabel lblNewLabel_3_1 = new JLabel("\u4E0A\u4E00\u9996");
	        lblNewLabel_3_1.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		playmusic.action("关闭");
	        		i--;
	        		if(i == -1)
	        			i = songorder.length - 1;
	        		playmusic.Play("music\\"+songorder[i]+".wav");
	        		playmusic.action("播放");
	        		textField.setText(songorder[i]);
	        		lblNewLabel_3_1_1.setIcon(new ImageIcon("image\\暂停播放.png"));
	        		Playing = false;
	        	}
	        });
	        lblNewLabel_3_1.setIcon(new ImageIcon("image\\上一首.png"));
	        lblNewLabel_3_1.setHorizontalAlignment(SwingConstants.CENTER);
	        lblNewLabel_3_1.setBounds(155, 90, 75, 75);
	        panel_1.add(lblNewLabel_3_1);
	        
	 
	        
	        JLabel lblNewLabel_3_3 = new JLabel("\u4E0B\u4E00\u9996");
	        lblNewLabel_3_3.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		playmusic.action("关闭");
	        		i++;
	        		if(i == songorder.length)
	        			i = 0;
	        		playmusic.Play("music\\"+songorder[i]+".wav");
	        		playmusic.action("播放");
	        		textField.setText(songorder[i]);
	        		lblNewLabel_3_1_1.setIcon(new ImageIcon("image\\暂停播放.png"));
	        		Playing = false;
	        		
	        	}
	        });
	        lblNewLabel_3_3.setIcon(new ImageIcon("image\\切歌.png"));
	        lblNewLabel_3_3.setHorizontalAlignment(SwingConstants.CENTER);
	        lblNewLabel_3_3.setBounds(370, 90, 75, 75);
	        panel_1.add(lblNewLabel_3_3);
	        
	        
	       
	        
	        JLabel lblNewLabel_3_3_1 = new JLabel("\u8FD4\u56DE");
	        lblNewLabel_3_3_1.setIcon(new ImageIcon("image\\返回.png"));
	        lblNewLabel_3_3_1.setHorizontalAlignment(SwingConstants.CENTER);
	        lblNewLabel_3_3_1.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		playmusic.action("关闭");
	        		new Play();
	        		dispose();
	        	}
	        });
	        lblNewLabel_3_3_1.setBounds(481, 90, 75, 75);
	        panel_1.add(lblNewLabel_3_3_1);
	        
	        JLabel lblNewLabel_1 = new JLabel("\u6B63\u5728\u64AD\u653E\u7684\u662F;");
	        lblNewLabel_1.setForeground(Color.BLACK);
	        lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 18));
	        lblNewLabel_1.setBounds(39, 17, 127, 49);
	        panel_1.add(lblNewLabel_1);
	        
	        textField = new JTextField();
	        textField.setForeground(new Color(255, 69, 0));
	        textField.setFont(new Font("宋体", Font.PLAIN, 18));
	        textField.setBounds(192, 20, 374, 43);
	        panel_1.add(textField);
	        textField.setColumns(10);
	        setVisible(true);
	        
	}
}
