package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.Icon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Server extends JFrame {

	private JPanel contentPane;
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Server frame = new Server();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */
	/**
	 * Create the frame.
	 */
	public Server() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 506, 508);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 492, 98);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
		lblNewLabel.setBounds(0, 0, 492, 98);
		panel.add(lblNewLabel);
		tabbedPane.setBounds(0, 99, 492, 289);
		contentPane.add(tabbedPane);
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"\u7092\u7530\u87BA  50/\u76D8", "\u7092\u725B\u8089  100/\u76D8"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		tabbedPane.addTab("\u98DF\u54C1", null, list, null);
		
		JList list_1 = new JList();
		list_1.setModel(new AbstractListModel() {
			String[] values = new String[] {"\u80A5\u4ED4\u5FEB\u4E50\u6C34 10/\u74F6", "\u96F7\u78A7        8/\u74F6", "\u79BB\u6CC9\u5564\u9152   20/\u74F6", "\u8461\u8404\u9152     15/\u74F6"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		tabbedPane.addTab("\u9152\u6C34\u996E\u54C1", null, list_1, null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(-10, 386, 502, 90);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnNewButton = new JButton("\u8BF7\u6C42\u670D\u52A1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(0, 0, 236, 40);
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u8FD4\u56DE\u754C\u9762");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(235, 0, 267, 40);
		panel_1.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("\u67E5\u770B\u6D88\u8D39\u65F6\u95F4");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.setBounds(10, 40, 226, 40);
		panel_1.add(btnNewButton_2);
		
		JButton btnNewButton_1_1 = new JButton("\u6536\u8D39\u6807\u51C6");
		btnNewButton_1_1.setBounds(235, 40, 267, 40);
		panel_1.add(btnNewButton_1_1);
		setVisible(true);
	}
}
