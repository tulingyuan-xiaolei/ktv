package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SongRank extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { SongRank frame = new SongRank();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public SongRank() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 645, 691);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JPanel panel = new JPanel();
        panel.setBounds(-10, 10, 631, 664);
        contentPane.add(panel);
        panel.setLayout(null);
        
        JList list_1 = new JList();
        list_1.setModel(new AbstractListModel() {
        	String[] values = new String[] {"1. \u4E11\u516B\u602A \u859B\u4E4B\u8C26", "2.\u6674\u5929    \u5468\u6770\u4F26"};
        	public int getSize() {
        		return values.length;
        	}
        	public Object getElementAt(int index) {
        		return values[index];
        	}
        });
        list_1.setBounds(0, 136, 631, 432);
        panel.add(list_1);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("D:\\\u684C\u9762\\image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
        lblNewLabel.setBounds(0, 0, 631, 139);
        panel.add(lblNewLabel);
        
        JButton btnNewButton = new JButton("\u8FD4\u56DE\u4E3B\u754C\u9762");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        btnNewButton.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		new Play();
        		dispose();
        	}
        });
        btnNewButton.setBounds(341, 588, 167, 51);
        panel.add(btnNewButton);
        
        JButton btnNewButton_1 = new JButton("\u52A0\u5165\u5DF2\u70B9");
        btnNewButton_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        btnNewButton_1.setBounds(101, 588, 167, 51);
        panel.add(btnNewButton_1);
        setVisible(true);
	}
}
