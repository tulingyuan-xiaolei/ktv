package KTV;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SqlServer;

import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Windows extends JFrame {

	private JPanel contentPane;
    Add_admin admin;
    public void setAdmin(Add_admin admin) {//保存admin的数据并传给windows
    	this.admin = admin;
    	
    }
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					windos frame = new windos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Windows() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 45, 50);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("\u4F18\u5148");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewLabel.setBackground(Color.gray);
				lblNewLabel.setOpaque(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNewLabel.setBackground(new Color( 240,240,240));
				lblNewLabel.setOpaque(true);
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				int index1 =  Add_admin.index1;
				admin.index(index1);
				dispose();
			}
		});
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u5220\u9664");
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewLabel_1.setBackground(Color.gray);
				lblNewLabel_1.setOpaque(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNewLabel_1.setBackground(new Color( 240,240,240));
				lblNewLabel_1.setOpaque(true);
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				int index1 =  Add_admin.index1;
				String name = admin.getName();
				SqlServer sqlserver = new SqlServer();
				sqlserver.DeleteSongList(name);
				String[] values  = admin.getValues();
				admin.list.setListData(values);
				dispose();
			}
		});
		panel.add(lblNewLabel_1);
		setUndecorated(true);
		setVisible(true);
	}
}
