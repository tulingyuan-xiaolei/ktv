package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.Song;
import SQL.SqlServer;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JScrollBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class LanguageSearch extends JFrame {

	private JPanel contentPane;
	JList list;
	String name;
	String singer;
	String language;
	static int index4;
	/**
	 * @wbp.nonvisual location=-23,720
	 */
	private final JScrollBar scrollBar = new JScrollBar();

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { LanguageSearch frame = new
	 * LanguageSearch(); frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */
	/**
	 * Create the frame.
	 */
	
	public String[] getValues() {//获取列表
		SqlServer sqlServer = new SqlServer();
		List<Song> sList = sqlServer.getSongList();
		String str[] = new String[sList.size()];
		Song s = null;
		for(int i=0;i<sList.size();i++) {
			s = sList.get(i);
			str[i]  =s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
			
		}
		return str;
	
	}
	
    public String getName() {
    	return name;
    }
    public String getSinger() {
    	return singer;
    }
    public String getLanguage() {
    	return language;
    }
	public LanguageSearch() {
		LanguageSearch languageseacrh = this;
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 645, 691);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 631, 131);
        contentPane.add(panel);
        panel.setLayout(null);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
        lblNewLabel.setBounds(0, 0, 631, 131);
        panel.add(lblNewLabel);
        
        JPanel panel_1 = new JPanel();
        panel_1.setBounds(0, 134, 631, 394);
        contentPane.add(panel_1);
        panel_1.setLayout(null);
        
        list = new JList();
        list.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		index4= list.getSelectedIndex();
        		int x = e.getX();
				int y = e.getY();
				int rx = x + 10 + 100 +10;
				int ry = y + 52 + 100 +110;
        		WindowsAdd winadd= new WindowsAdd();
        		winadd.setLocation(rx,ry);
        		
 
        		Song s = null;
        		List<Song> sList= list.getSelectedValuesList();
        		String string = sList.toString();
        		String regex = "\\s+";
        		
        		String[] name1  = string.split(regex);
        		name = name1[0].substring(1,name1[0].length());
        		singer = name1[1];
        		language = name1[2].substring(0,name1[2].length()-1);
        		winadd.setlanguageSearch(languageseacrh);//使用windowsadd的方法传参 
        		winadd.getFlag(3);
        	}
        });
        list.setFont(new Font("宋体", Font.PLAIN, 18));
        list.setModel(new AbstractListModel() {//获取全部
			
			String[] values  = getValues();
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				
				return values[index];
			}
			
		});
        list.setBounds(0, 0, 631, 394);
        panel_1.add(list);
        
        JPanel panel_2 = new JPanel();
        panel_2.setBackground(Color.WHITE);
        panel_2.setBounds(0, 530, 631, 124);
        contentPane.add(panel_2);
        panel_2.setLayout(null);
        
        JLabel lblNewLabel_1 = new JLabel("New label");
        lblNewLabel_1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		Song song = new Song();
        		SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchLanguage("国语");;
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        lblNewLabel_1.setIcon(new ImageIcon("image\\国语.png"));
        lblNewLabel_1.setBounds(51, 10, 75, 75);
        panel_2.add(lblNewLabel_1);
        
        JLabel lblNewLabel_1_1 = new JLabel("New label");
        lblNewLabel_1_1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		Song song = new Song();
        		SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchLanguage("英语");;
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        lblNewLabel_1_1.setIcon(new ImageIcon("image\\欧美.png"));
        lblNewLabel_1_1.setBounds(195, 10, 75, 75);
        panel_2.add(lblNewLabel_1_1);
        
        JLabel lblNewLabel_1_1_1 = new JLabel("New label");
        lblNewLabel_1_1_1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		Song song = new Song();
        		SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchLanguage("日语");;
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        lblNewLabel_1_1_1.setIcon(new ImageIcon("image\\日语.png"));
        lblNewLabel_1_1_1.setBounds(363, 10, 75, 75);
        panel_2.add(lblNewLabel_1_1_1);
        
        JLabel lblNewLabel_1_1_1_1 = new JLabel("New label");
        lblNewLabel_1_1_1_1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		Song song = new Song();
        		SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchLanguage("韩语");;
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        lblNewLabel_1_1_1_1.setIcon(new ImageIcon("image\\韩语.png"));
        lblNewLabel_1_1_1_1.setBounds(512, 10, 75, 75);
        panel_2.add(lblNewLabel_1_1_1_1);
        
        JButton btnNewButton = new JButton("\u8FD4\u56DE");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		new Play();
        		dispose();
        	}
        });
        btnNewButton.setBounds(257, 95, 97, 29);
        panel_2.add(btnNewButton);
        setVisible(true);
	}

}
