package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.Song;
import SQL.SqlServer;

import javax.swing.JLabel;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SongSearch extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	JList list;
	String name;
	String singer;
	String language;
	static int index2;
	/**
	 * Launch the application.
	 */
	
	 
	/**
	 * Create the frame.
	 */
	public String[] getValues() {//获取列表
		SqlServer sqlServer = new SqlServer();
		List<Song> sList = sqlServer.getSongList();
		String str[] = new String[sList.size()];
		Song s = null;
		for(int i=0;i<sList.size();i++) {
			s = sList.get(i);
			str[i]  =s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
			
		}
		return str;
	
	}
	
    public String getName() {
    	return name;
    }
    public String getSinger() {
    	return singer;
    }
    public String getLanguage() {
    	return language;
    }
	
	public SongSearch() {
		SongSearch songsearch = this;//保存SongSearch的数据
        StringBuilder strbuild = new StringBuilder();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 645, 691);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("image\\ed7c74295284a4bdcb1a73be3a999102(1).jpg"));
        lblNewLabel.setBounds(0, 0, 631, 132);
        contentPane.add(lblNewLabel);
        
        JPanel panel = new JPanel();
        panel.setBounds(0, 132, 631, 300);
        contentPane.add(panel);
        panel.setLayout(null);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(0, 0, 631, 300);
        panel.add(scrollPane);
        
        list = new JList();
        list.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        	    index2= list.getSelectedIndex();
        		int x = e.getX();
				int y = e.getY();
				int rx = x + 10 + 100 +10;
				int ry = y + 52 + 100 +110;
        		WindowsAdd winadd= new WindowsAdd();
        		winadd.setLocation(rx,ry);
        		
        		Song s = null;
        		List<Song> sList= list.getSelectedValuesList();
        		String string = sList.toString();
        		String regex = "\\s+";
        		
        		String[] name1  = string.split(regex);
        		name = name1[0].substring(1,name1[0].length());
        		singer = name1[1];
        		language = name1[2].substring(0,name1[2].length()-1);
        		winadd.setSongSearch(songsearch);//使用windowsadd的方法传参 
        		winadd.getFlag(1);
        	}
        });
        scrollPane.setViewportView(list);
        list.setFont(new Font("宋体", Font.PLAIN, 18));
        list.setModel(new AbstractListModel() {//获取全部
			
			String[] values  = getValues();
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				
				return values[index];
			}
			
		});

        JPanel panel_1 = new JPanel();
        panel_1.setForeground(Color.LIGHT_GRAY);
        panel_1.setBounds(0, 434, 631, 220);
        contentPane.add(panel_1);
        panel_1.setLayout(null);
        
        JButton btnNewButton = new JButton("Q");
        btnNewButton.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("Q");
				String str = strbuild.toString();
				textField.setText(str);
			
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        btnNewButton.setBounds(10, 36, 52, 52);
        panel_1.add(btnNewButton);
        
        JButton btnNewButton_1 = new JButton("W");
        btnNewButton_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("W");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_1.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_1.setBounds(79, 36, 52, 52);
        panel_1.add(btnNewButton_1);
        
        JButton btnNewButton_2 = new JButton("E");
        btnNewButton_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("E");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_2.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_2.setBounds(141, 36, 52, 52);
        panel_1.add(btnNewButton_2);
        
        JButton btnNewButton_3 = new JButton("R");
        btnNewButton_3.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("R");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_3.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_3.setBounds(203, 36, 52, 52);
        panel_1.add(btnNewButton_3);
        
        JButton btnNewButton_4 = new JButton("T");
        btnNewButton_4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("T");
				String str = strbuild.toString();
				textField.setText(str);
				SqlServer sqlsearch = new SqlServer();
				sqlsearch.getSearchSong(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_4.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_4.setBounds(265, 36, 52, 52);
        panel_1.add(btnNewButton_4);
        
        JButton btnNewButton_5 = new JButton("U");
        btnNewButton_5.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("U");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_5.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_5.setBounds(389, 36, 52, 52);
        panel_1.add(btnNewButton_5);
        
        JButton btnNewButton_6 = new JButton("I");
        btnNewButton_6.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("I");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_6.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_6.setBounds(451, 36, 52, 52);
        panel_1.add(btnNewButton_6);
        
        JButton btnO = new JButton("O");
        btnO.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("O");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnO.setFont(new Font("宋体", Font.PLAIN, 24));
        btnO.setBounds(513, 36, 52, 52);
        panel_1.add(btnO);
        
        JButton btnP = new JButton("P");
        btnP.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("P");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnP.setFont(new Font("宋体", Font.PLAIN, 24));
        btnP.setBounds(569, 36, 52, 52);
        panel_1.add(btnP);
        
        JButton btnNewButton_9 = new JButton("Y");
        btnNewButton_9.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		    strbuild.append("Y");
    				String str = strbuild.toString();
    				textField.setText(str);
    				Song song = new Song();
    				SqlServer sqlServer = new SqlServer();
    				List<Song> sList = sqlServer.getSearchSong(str);
    				
    				String str1[] = new String[sList.size()];
    				
    				Song s = null;
    				for(int i=0;i<sList.size();i++) {
    					s = sList.get(i);
    					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
    					
    					
    				}
    				list.setListData(str1);
        	}
        });
        btnNewButton_9.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_9.setBounds(327, 36, 52, 52);
        panel_1.add(btnNewButton_9);
        
        JButton btnA = new JButton("A");
        btnA.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("A");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnA.setFont(new Font("宋体", Font.PLAIN, 24));
        btnA.setBounds(24, 98, 52, 52);
        panel_1.add(btnA);
        
        JButton btnNewButton_7_1 = new JButton("S");
        btnNewButton_7_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("S");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_1.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_1.setBounds(86, 98, 52, 52);
        panel_1.add(btnNewButton_7_1);
        
        JButton btnNewButton_7_2 = new JButton("D");
        btnNewButton_7_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("D");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_2.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_2.setBounds(156, 98, 52, 52);
        panel_1.add(btnNewButton_7_2);
        
        JButton btnNewButton_7_3 = new JButton("F");
        btnNewButton_7_3.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("F");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_3.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_3.setBounds(218, 98, 52, 52);
        panel_1.add(btnNewButton_7_3);
        
        JButton btnNewButton_7_4 = new JButton("G");
        btnNewButton_7_4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("G");
				String str = strbuild.toString();
				textField.setText(str);
				
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_4.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_4.setBounds(280, 98, 52, 52);
        panel_1.add(btnNewButton_7_4);
        
        JButton btnNewButton_7_5 = new JButton("H");
        btnNewButton_7_5.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("H");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_5.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_5.setBounds(342, 98, 52, 52);
        panel_1.add(btnNewButton_7_5);
        
        JButton btnNewButton_7_6 = new JButton("J");
        btnNewButton_7_6.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("J");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_6.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_6.setBounds(404, 98, 52, 52);
        panel_1.add(btnNewButton_7_6);
        
        JButton btnNewButton_7_7 = new JButton("K");
        btnNewButton_7_7.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("K");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_7_7.setFont(new Font("宋体", Font.PLAIN, 24));
        btnNewButton_7_7.setBounds(462, 98, 52, 52);
        panel_1.add(btnNewButton_7_7);
        
        JButton btnA_1 = new JButton("L");
        btnA_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("L");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnA_1.setFont(new Font("宋体", Font.PLAIN, 24));
        btnA_1.setBounds(534, 98, 52, 52);
        panel_1.add(btnA_1);
        
        JButton btnZ = new JButton("Z");
        btnZ.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("Z");
				String str = strbuild.toString();
				textField.setText(str);

				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnZ.setFont(new Font("宋体", Font.PLAIN, 24));
        btnZ.setBounds(50, 160, 52, 52);
        panel_1.add(btnZ);
        
        JButton btnX = new JButton("X");
        btnX.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("X");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnX.setFont(new Font("宋体", Font.PLAIN, 24));
        btnX.setBounds(112, 160, 52, 52);
        panel_1.add(btnX);
        
        JButton btnC = new JButton("C");
        btnC.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("C");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        btnC.setFont(new Font("宋体", Font.PLAIN, 24));
        btnC.setBounds(180, 160, 52, 52);
        panel_1.add(btnC);
        
        JButton btnV = new JButton("V");
        btnV.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("V");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnV.setFont(new Font("宋体", Font.PLAIN, 24));
        btnV.setBounds(242, 160, 52, 52);
        panel_1.add(btnV);
        
        JButton btnB = new JButton("B");
        btnB.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("B");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnB.setFont(new Font("宋体", Font.PLAIN, 24));
        btnB.setBounds(304, 160, 52, 52);
        panel_1.add(btnB);
        
        JButton btnN = new JButton("N");
        btnN.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("N");
				String str = strbuild.toString();
				textField.setText(str);

				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnN.setFont(new Font("宋体", Font.PLAIN, 24));
        btnN.setBounds(360, 160, 52, 52);
        panel_1.add(btnN);
        
        JButton btnM = new JButton("M");
        btnM.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.append("M");
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
				
				}
				list.setListData(str1);
        	}
        });
        btnM.setFont(new Font("宋体", Font.PLAIN, 24));
        btnM.setBounds(422, 160, 52, 52);
        panel_1.add(btnM);
        
        JLabel lblNewLabel_1 = new JLabel("\u6B4C\u66F2\u9996\u5B57\u6BCD");
        lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 21));
        lblNewLabel_1.setBounds(0, 1, 114, 28);
        panel_1.add(lblNewLabel_1);
        
        JButton btnNewButton_7 = new JButton("\u9000\u683C");
        btnNewButton_7.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		try {
        		strbuild.deleteCharAt(strbuild.length()-1);
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        		}catch(Exception e1) {
        			JOptionPane.showMessageDialog(panel_1,"输入框已为空","消息对话框",JOptionPane.WARNING_MESSAGE);
        		}
        	}
        });
        btnNewButton_7.setBounds(484, 160, 104, 52);
        panel_1.add(btnNewButton_7);
        
        JButton btnNewButton_8 = new JButton("\u6E05\u7A7A");
        btnNewButton_8.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		strbuild.delete(0, strbuild.length());
				String str = strbuild.toString();
				textField.setText(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSearchSong(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
        	}
        });
        btnNewButton_8.setBounds(370, 0, 123, 35);
        panel_1.add(btnNewButton_8);
        
        JButton btnNewButton_10 = new JButton("\u8FD4\u56DE");
        btnNewButton_10.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		new Play();
        		dispose();
        	}
        });
        btnNewButton_10.setBounds(503, 0, 117, 33);
        panel_1.add(btnNewButton_10);
        
        textField = new JTextField();
        textField.setBounds(124, 0, 223, 31);
        panel_1.add(textField);
        textField.setColumns(10);
		setVisible(true);
		
	}
}
