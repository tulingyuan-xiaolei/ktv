package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.border.EmptyBorder;

import SQL.SqlOrder;
import SQL.Sqluser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import java.awt.Color;
public class Login extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("\u6D88\u8D39\u8005", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u5305\u53A2\u9009\u62E9");
		lblNewLabel.setBounds(70, 16, 78, 40);
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("\u6536\u8D39\u6807\u51C6");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Charges();
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(126, 66, 126, 41);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u5F00\u59CB\u5531\u6B4C");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Play();
				SqlOrder sqlorder = new SqlOrder();
				sqlorder.Delete();
			}
		});
		btnNewButton_1.setBounds(126, 117, 126, 41);
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_1_1 = new JButton("\u9000\u51FA");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1_1.setBounds(126, 168, 126, 41);
		panel.add(btnNewButton_1_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"\u57FA\u7840\u5305", "\u8C6A\u534E\u5305", "\u603B\u7EDF\u5305"}));
		comboBox.setBounds(214, 19, 100, 37);
		panel.add(comboBox);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("\u7BA1\u7406\u5458", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("\u8D26\u53F7");
		lblNewLabel_1.setBounds(117, 33, 39, 19);
		panel_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u5BC6\u7801");
		lblNewLabel_2.setBounds(117, 94, 35, 15);
		panel_1.add(lblNewLabel_2);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(177, 91, 127, 21);
		panel_1.add(passwordField);
		
		JLabel lblNewLabel_3 = new JLabel(" ");
		lblNewLabel_3.setForeground(Color.RED);
		lblNewLabel_3.setBounds(150, 134, 127, 15);
		panel_1.add(lblNewLabel_3);
		
		JButton btnNewButton_2 = new JButton("\u767B\u9646");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Sqluser  sqluser = new Sqluser();
				String str2 = new String(passwordField.getPassword());
				String str =  sqluser.check(textField.getText(),str2);
				
				if(str.equals("登录成功")) {
				
					new Add_admin();
				    
				}else {
					
				JOptionPane.showMessageDialog(panel_1,str,"消息对话框",JOptionPane.WARNING_MESSAGE);
						
				}
					
			

			}
		});
		btnNewButton_2.setBounds(140, 159, 97, 23);
		panel_1.add(btnNewButton_2);
		
		textField = new JTextField();
		textField.setBounds(177, 32, 127, 21);
		panel_1.add(textField);
		textField.setColumns(10);
		
		
	}
}
