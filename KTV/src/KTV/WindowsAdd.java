package KTV;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SqlOrder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WindowsAdd extends JFrame {
    SongSearch songsearch;
    SingerSearch singersearch;
    LanguageSearch languagesearch;
    int flag;
	private JPanel contentPane;
    public void setSongSearch(SongSearch songsearch) {//把songsearch的信息传过来
    	this.songsearch = songsearch;
    }
    public void setSingerSearch(SingerSearch singersearch) {//把singersearch的信息传过来
    	this.singersearch = singersearch;
    }
    public void setlanguageSearch(LanguageSearch languagesearch) {//把langguagesearch的信息传过来
    	this.languagesearch = languagesearch;
    }
    
    public void getFlag(int flag) {//
    	this.flag = flag;
    }
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowsAdd frame = new WindowsAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public WindowsAdd() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 60, 55);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		JLabel lblNewLabel_1 = new JLabel("\u52A0\u5165\u6B4C\u5355");
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SqlOrder sqlorder = new SqlOrder();
				if(flag == 1) {
				   String name = songsearch.getName();
				   String singer = songsearch.getSinger();
			       String language = songsearch.getLanguage();
				   sqlorder.Add(name, singer, language);
				}else if(flag == 2)	{
					String name1 = singersearch.getName();
					String singer1 = singersearch.getSinger();
				    String language1 = singersearch.getLanguage();
					sqlorder.Add(name1, singer1, language1);
				}else if(flag == 3) {
					String name2 = languagesearch.getName();
					String singer2 = languagesearch.getSinger();
				    String language2 = languagesearch.getLanguage();
					sqlorder.Add(name2, singer2, language2);
				}
					
				JOptionPane.showMessageDialog(panel,"添加成功","消息对话框",JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewLabel_1.setBackground(Color.gray);
				lblNewLabel_1.setOpaque(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNewLabel_1.setBackground(new Color( 240,240,240));
				lblNewLabel_1.setOpaque(true);
			}
		});
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("\u53D6\u6D88");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewLabel.setBackground(Color.gray);
				lblNewLabel.setOpaque(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNewLabel.setBackground(new Color( 240,240,240));
				lblNewLabel.setOpaque(true);
			}
		});
		panel.add(lblNewLabel);
		setUndecorated(true);
		setVisible(true);
	}

}
