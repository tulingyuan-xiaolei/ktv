package KTV;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SqlOrder;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.JTextArea;

public class Play extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Play frame = new Play();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public Play() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 431, 258);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("image\\t013cff0866fe0a0841.jpg"));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(4, -5, 417, 103);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				new ClassfySearch();
				
				dispose();
			}
		});
		lblNewLabel_1.setIcon(new ImageIcon("image\\分类.png"));
		lblNewLabel_1.setBounds(4, 148, 75, 75);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u70B9\u6B4C");
		lblNewLabel_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new ClassfySearch();
				dispose();
			}
		});
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 14));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(10, 233, 69, 15);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1_1 = new JLabel("New label");
		lblNewLabel_1_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
				new SongOder();
				dispose();
				}catch(Exception e1) {
					JOptionPane.showMessageDialog(panel,"先添加歌曲","消息对话框",JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		lblNewLabel_1_1.setIcon(new ImageIcon("image\\已点歌曲.png"));
		lblNewLabel_1_1.setBounds(101, 148, 75, 75);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("New label");
		lblNewLabel_1_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new SongRank();
				dispose();
			}
		});
		lblNewLabel_1_2.setIcon(new ImageIcon("image\\排行.png"));
		lblNewLabel_1_2.setBounds(197, 148, 75, 75);
		panel.add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("\u5DF2\u70B9\u6B4C\u66F2");
		lblNewLabel_2_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					new SongOder();
					dispose();
					}catch(Exception e1) {
						JOptionPane.showMessageDialog(panel,"先添加歌曲","消息对话框",JOptionPane.WARNING_MESSAGE);
					}
			}
		});
		lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1.setFont(new Font("宋体", Font.PLAIN, 14));
		lblNewLabel_2_1.setBounds(97, 233, 69, 15);
		panel.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("\u6392\u884C");
		lblNewLabel_2_1_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new SongRank();
			}
		});
		lblNewLabel_2_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1_1.setFont(new Font("宋体", Font.PLAIN, 14));
		lblNewLabel_2_1_1.setBounds(203, 233, 69, 15);
		panel.add(lblNewLabel_2_1_1);
		
		JLabel lblNewLabel_1_2_1 = new JLabel("New label");
		lblNewLabel_1_2_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Server();
			}
		});
		lblNewLabel_1_2_1.setIcon(new ImageIcon("image\\酒水服务.png"));
		lblNewLabel_1_2_1.setBounds(321, 148, 75, 75);
		panel.add(lblNewLabel_1_2_1);
		
		JLabel lblNewLabel_2_1_1_1 = new JLabel("\u9152\u6C34\u670D\u52A1");
		lblNewLabel_2_1_1_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Server();
			}
		});
		lblNewLabel_2_1_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1_1_1.setFont(new Font("宋体", Font.PLAIN, 14));
		lblNewLabel_2_1_1_1.setBounds(327, 233, 69, 15);
		panel.add(lblNewLabel_2_1_1_1);
		setVisible(true);
	}
}
