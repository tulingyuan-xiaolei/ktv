package KTV;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.Song;
import SQL.SqlServer;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import java.awt.Font;

public class Add_admin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	JList list;
	/**
	 * Launch the application.
	 */
	
	 
	/**
	 * Create the frame.
	 */
	
	
	//新建方法
			public void index(int index1) {//优先
				String str[] = this.getValues();
			    String temp = new String();
			    for(int i= index1;i>0;i--) {
			      temp = str[i-1];
			      str[i-1] = str[i];
			      str[i] = temp;
       			 
			  }
			    list.setListData(str);
			}
			public String[] getValues() {//获取列表
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSongList();
				String str[] = new String[sList.size()];
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str[i]  =s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
				}
				return str;
			
			}
			public String getName() {//删除
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSongList();
				String str[] = new String[sList.size()];
				Song s = null;
				s = sList.get(index1);
			    String name  =s.getName();
			    return name;
			}
			static int index1;
			
	public Add_admin() {
		Add_admin admin = this;//保存数据
		StringBuilder strbuild = new StringBuilder();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 455, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 5, 156, 32);
		panel.add(textField);
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 52, 411, 308);
		panel.add(scrollPane);
		
		 list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				admin.index1 = list.getSelectedIndex();
			    
				int x = e.getX();
				int y = e.getY();
				int rx = x + 10 + 100 +10;
				int ry = y + 52 + 100 +35;
				Windows win= new Windows();
				win.setLocation(rx,ry);
				win.setAdmin(admin);//调用Windows的方法传参
				//String[] values  = getValues();
				//list.setListData(values);
				
			}
		});
		list.setFont(new Font("宋体", Font.PLAIN, 18));
		scrollPane.setViewportView(list);
		list.setModel(new AbstractListModel() {
			
			String[] values  = getValues();
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				
				return values[index];
			}
			
		});
		
		
		JButton btnNewButton = new JButton("\u67E5\u8BE2");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strbuild.delete(0, strbuild.length());
				strbuild.append(textField.getText());
				String str = strbuild.toString();
				textField.setText(str);
				SqlServer sqlsearch = new SqlServer();
				sqlsearch.getSearchSong(str);
				Song song = new Song();
				SqlServer sqlServer = new SqlServer();
				List<Song> sList = sqlServer.getSongListName(str);
				
				String str1[] = new String[sList.size()];
				
				Song s = null;
				for(int i=0;i<sList.size();i++) {
					s = sList.get(i);
					str1[i]  = s.getName()+"  "+s.getSinger()+"  "+s.getlanguage();
					
					
				}
				list.setListData(str1);
			}
		});
		btnNewButton.setBounds(176, -1, 122, 42);
		panel.add(btnNewButton);
			
		JButton btnNewButton_1 = new JButton("\u6DFB\u52A0\u6B4C\u66F2");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Add_song();
				dispose();
			}
		});
		btnNewButton_1.setBounds(298, -1, 123, 42);
		panel.add(btnNewButton_1);
		
		
		JButton btnNewButton_2 = new JButton("\u9000\u51FA");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_2.setBounds(132, 364, 118, 51);
		panel.add(btnNewButton_2);
		
		
		setVisible(true);
	}
}
